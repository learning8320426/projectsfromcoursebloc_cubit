part of 'counter_bloc.dart';

class CounterState extends Equatable {
  final int counter;
  final int sizeIncrement;
  CounterState({
    required this.counter,
    required this.sizeIncrement,
  });

  factory CounterState.initial() {
    return CounterState(counter: 0, sizeIncrement: 1);
  }

  @override
  List<Object> get props => [counter, sizeIncrement];

  @override
  String toString() => 'CounterState(counter: $counter)';

  CounterState copyWith({
    int? counter,
    int? sizeIncrement,
  }) {
    return CounterState(
      counter: counter ?? this.counter,
      sizeIncrement: sizeIncrement ?? this.sizeIncrement,
    );
  }
}
