import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'blocs/color/color_bloc.dart';
import 'blocs/counter/counter_bloc.dart';

void main() {
  runApp(const MyApp());
}
// //BLOC2BLOC_STREAM_SUBSCRIPTION
// class MyApp extends StatelessWidget {
//   const MyApp({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return MultiBlocProvider(
//       providers: [
//         BlocProvider<ColorBloc>(
//           create: (context) => ColorBloc(),
//         ),
//         BlocProvider<CounterBloc>(
//           create: (context) => CounterBloc(
//             colorBloc: context.read<ColorBloc>(),
//           ),
//         ),
//       ],
//       child: MaterialApp(
//         title: 'bloc2bloc',
//         debugShowCheckedModeBanner: false,
//         theme: ThemeData(
//           primarySwatch: Colors.blue,
//         ),
//         home: const MyHomePage(),
//       ),
//     );
//   }
// }

// class MyHomePage extends StatelessWidget {
//   const MyHomePage({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: context.watch<ColorBloc>().state.color,
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: [
//             ElevatedButton(
//               child: const Text(
//                 'Change Color',
//                 style: TextStyle(fontSize: 24.0),
//               ),
//               onPressed: () {
//                 context.read<ColorBloc>().add(ChangeColorEvent());
//               },
//             ),
//             const SizedBox(height: 20.0),
//             Text(
//               '${context.watch<CounterBloc>().state.counter}',
//               style: const TextStyle(
//                 fontSize: 52.0,
//                 fontWeight: FontWeight.bold,
//                 color: Colors.white,
//               ),
//             ),
//             const SizedBox(height: 20.0),
//             ElevatedButton(
//               child: const Text(
//                 'Increment Counter',
//                 style: TextStyle(fontSize: 24.0),
//               ),
//               onPressed: () {
//                 context.read<CounterBloc>().add(ChangeCounterEvent());
//               },
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

//BLOC2BLOC BLOC LISTENER
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ColorBloc>(
          create: (context) => ColorBloc(),
        ),
        BlocProvider<CounterBloc>(
          create: (context) => CounterBloc(),
        ),
      ],
      child: MaterialApp(
        title: 'bloc2bloc',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<ColorBloc, ColorState>(
      listener: (beforState, colorState) {
        if (colorState.color == Colors.red) {
          context
              .read<CounterBloc>()
              .add(const ChangeIncrementSizeEvent(incrementSize: 1));
        } else if (colorState.color == Colors.green) {
          context
              .read<CounterBloc>()
              .add(const ChangeIncrementSizeEvent(incrementSize: 10));
        } else if (colorState.color == Colors.blue) {
          context
              .read<CounterBloc>()
              .add(const ChangeIncrementSizeEvent(incrementSize: 100));
        } else if (colorState.color == Colors.black) {
          context
              .read<CounterBloc>()
              .add(const ChangeIncrementSizeEvent(incrementSize: -100));
          context.read<CounterBloc>().add(const ChangeCounterEvent());
        }
      },
      child: Scaffold(
        backgroundColor: context.watch<ColorBloc>().state.color,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                child: const Text(
                  'Change Color',
                  style: TextStyle(fontSize: 24.0),
                ),
                onPressed: () {
                  context.read<ColorBloc>().add(ChangeColorEvent());
                },
              ),
              const SizedBox(height: 20.0),
              Text(
                '${context.watch<CounterBloc>().state.counter}',
                style: const TextStyle(
                  fontSize: 52.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
              const SizedBox(height: 20.0),
              ElevatedButton(
                child: const Text(
                  'Increment Counter',
                  style: TextStyle(fontSize: 24.0),
                ),
                onPressed: () {
                  context.read<CounterBloc>().add(const ChangeCounterEvent());
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
