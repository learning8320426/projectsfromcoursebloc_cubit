import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'counter_bloc.freezed.dart';

part 'counter_event.dart';
part 'counter_state.dart';

class CounterBloc extends Bloc<CounterEvent, CounterState> {
  CounterBloc() : super(CounterState.loaded(counter: 0)) {
    on<CounterIncrementEvent>((event, emit) {
      final counterNow = state.counter;
      emit(CounterState.loaded(counter: counterNow + 1));
    });
    on<CounterDecrementEvent>((event, emit) {
      final counterNow = state.counter;
      emit(CounterState.loaded(counter: counterNow - 1));
    });
  }
}
