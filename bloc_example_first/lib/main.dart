//import 'package:bloc_example_first/cubits/bloc/counter_bloc.dart';
//import 'package:bloc_example_first/cubits/cubit/counter_cubit.dart';
import 'package:bloc_example_first/cubits/cubit/counter_cubit.dart';
import 'package:bloc_example_first/other_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

//import 'cubits/freezed_bloc/bloc/counter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CounterCubit>(
      create: (context) => CounterCubit(),
      //
      //   BlocProvider<CounterCubit>(
      // create: (context) => CounterCubit(),
      child: MaterialApp(
        title: 'My counter cubit',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
          primarySwatch: Colors.blue,
        ),
        home: const MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    var cubit = context.watch<CounterCubit>();
    // var bloc = context.watch<CounterBloc>();
    return Scaffold(
      body: BlocListener<CounterCubit, CounterState>(
        // listenWhen: (previousState, state) {
        //   return state.counter > 3;
        // },
        listener: (context, state) {
          if (state.counter == 3) {
            showDialog(
                context: context,
                builder: ((context) {
                  return AlertDialog(
                    content: Text('Counter is ${state.counter}'),
                  );
                }));
          } else if (state.counter == -1) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const OtherPage()));
          }
        },
        child: Builder(
          builder: (context) {
            return Center(
              child: Text(
                // '${bloc.state.counter}',
                '${context.watch<CounterCubit>().state.counter}',
                style: const TextStyle(fontSize: 52),
              ),
            );
          },
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: () {
              cubit.increment();
              //  bloc.add(CounterTapIncrement());
              // bloc.add(const CounterEvent.increment());
            },
            heroTag: 'increment',
            child: const Icon(Icons.add),
          ),
          const SizedBox(width: 10),
          FloatingActionButton(
            onPressed: () {
              cubit.decrement();
              // bloc.add(CounterTapDecrement());
              // bloc.add(const CounterEvent.decrement());
            },
            heroTag: 'decrement',
            child: const Icon(Icons.remove),
          ),
        ],

        //   child: BlocBuilder<CounterBloc, CounterState>(
        //     builder: (context, state) {
        //       return Text(
        //         '${state.counter}',
        //         // '${cubit.state.counter}',
        //         style: const TextStyle(fontSize: 52),
        //       );
        //     },
        //   ),
        // ),
        // floatingActionButton: Row(
        //   mainAxisAlignment: MainAxisAlignment.end,
        //   children: [
        //     FloatingActionButton(
        //       onPressed: () {
        //         context.read<CounterBloc>().add(const CounterEvent.increment());
        //       },
        //       heroTag: 'increment',
        //       child: const Icon(Icons.add),
        //     ),
        //     const SizedBox(width: 10),
        //     FloatingActionButton(
        //       onPressed: () {
        //         context.read<CounterBloc>().add(const CounterEvent.decrement());
        //         // cubit.decrement();
        //       },
        //       heroTag: 'decrement',
        //       child: const Icon(Icons.remove),
        //     ),
        //   ],
      ),
    );
  }
}
